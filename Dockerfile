################## BEGIN INSTALLATION ######################
FROM ubuntu

# Install general dependencies
RUN apt update
RUN apt install -y --fix-missing \
	sudo \
	git \
	cmake \
	build-essential \
	liblog4cxx-dev \
	curl \
	autoconf \
	automake \
	libtool \
	make \
	g++ \
	unzip \
	libboost-all-dev

RUN curl -OL https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.zip
RUN unzip protobuf-2.6.1 -d proto2
WORKDIR proto2/protobuf-2.6.1/

RUN ./configure
RUN make
RUN make check
RUN make install
RUN ldconfig # refresh shared library cache.

WORKDIR /
RUN git clone https://github.com/oktal/pistache.git
WORKDIR pistache
RUN mkdir build
WORKDIR build
RUN cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
RUN make
RUN sudo make install 

WORKDIR /
################## END INSTALLATION ######################

################## BEGIN CONFIGURING OAI ######################
# Create configuration folder
RUN git clone http://192.168.10.18:10080/root/flexran_controller.git

WORKDIR flexran_controller
RUN ./build_flexran_rtc.sh

CMD ./run_flexran_rtc.sh
################## END CONFIGURING OAI ######################
